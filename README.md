# Packer | MacOS

> Création et configuration d'une Machine Virtuelle VirtualBox sous Mac OS pour le développement.

[[_TOC_]]

## Dépendances

* [Packer](https://www.packer.io/downloads)
* [VirtalBox](https://www.virtualbox.org/wiki/Downloads) >= 6.1.14  
* Un Mac



## Prérequis

__Apple__ autorise la virtualisation mais sur du matériel Apple. De ce faite, cela ne permet pas l'installation d'instance virtualisé de MacOS si on ne dispose pas d'un matériel Apple.  
Donc il faut déjà disposer une machine sous MacOS.  
Il est quand même possible de virtualiser un MacOS mais uniquement pour un but de fournir un environnement de développement et de tests. 

### Depuis un MacOS

> non testé

Création d'une ISO bootable:  
```
# Create a macOS Big Sur Virtual Disk Image
hdiutil create -o /tmp/bigsur -size 10250m -volname bigsur -layout SPUD -fs HFS+J
# Mount this Image to macOS
hdiutil attach /tmp/bigsur.dmg -noverify -mountpoint /Volumes/bigsur
# Use macOS Big Sur Createinstallmedia Tool to create a Installer Image
sudo /Applications/Install\ macOS\ Beta.app/Contents/Resources/createinstallmedia --volume /Volumes/bigsur --nointeraction
# Unmount Volume Install macOS Beta
hdiutil eject -force /Volumes/Install\ macOS\ Beta
# Convert the bigsur.dmg to a bigsur.iso for Virtual Machine
hdiutil convert /tmp/bigsur.dmg -format UDTO -o ~/Desktop/bigsur.cdr
# Move and Rename bigsur Image to Desktop
mv ~/Desktop/bigsur.cdr ~/Desktop/bigsur.iso
```

Reste à voir comment automatiser l'install de l'iso...
Regarder par là -> [https://github.com/timsutton/osx-vm-templates](https://github.com/timsutton/osx-vm-templates)

### From scratch

> Version préféré pour le développement et les tests.

Le projet [macos-virtualbox](https://github.com/myspaghetti/macos-virtualbox) a pour but d'automatiser l'installation d'une version de MacOS dans une machine virtuelle VirtualBox.

Une version du script est disponible [pre\_script/macos-guest-virtualbox.sh](pre\_script/macos-guest-virtualbox.sh).  Il y a des dépendances, regarder dans le header du script.

Au jour d'aujourd'hui, le script permet d'automatiser la création de la machine virtuelle mais pas la configuration de l'OS.  Après l'execution du script, il faut démarrer la MV et la configurer (ssh + compte utilisateur vagrant/vagrant). Ensuite l'exporter en format __OVA__.

## Packer et Template JSON

__Packer__ nous permet de créer des machines identiques depuis une ISO (ou autre) pour de multiple platforms depuis un simple fichier (_Template_).  
Le fichier _Template_ est une fichier _json_ qui décrit comment créer la machine virtuelle.  cf : [packer\_macos.json](packer_macos.json).  

Les _Builders_ (Providers) sont réponsable de construire l'image de la machine pour une plateforme specifique (VMWare, VirutalBox, Azure, AWS...).  
Les _Provisioners_ installent et configurent une machine. __Packer__ permet du provisioning simple (copies fichier,script, install package), il est préférable d'utiliser un autre outils de provisioning type Vagrant, Ansible.  
La Communication se fait principalement via SSH.

Il faut ajouter ceci si l'hote n'est pas un intel:  
```
["modifyvm", "{{.Name}}", "--cpu-profile", "Intel Core i7-6700K"]
```

Le template [packer\_macos.json](packer_macos.json) permet de contruire une machine virtuelle VirtualBox sous MacOS depuis une ISO ou un OVA.

## Pre-Provisioning

Le script [scripts](install-xcode.sh) permet d'installer xcode et Command Line Tools depuis un _dmg_ ou _pix_.  
Pour ce faire, il faut se rendre sur [https://developer.apple.com/download/more/](https://developer.apple.com/download/more/) est télécharger la version necessaire. Actuellement le script install _Command\_Line\_Tools\_for\_Xcode\_11.5.dmg_ et _Xcode\_11.7.xip_.

Le script [install-homebrew.sh](scripts/install-homebrew.sh) installe homebrew.  
Le script [add-sudouser.sh](scripts/add-sudouser.sh) ajoute une régle sudoers pour l'utilisateur vagrant avec NOPASSWD.

## Build

Avec une ISO généré depuis un mac.
```
packer build --only=virtualbox-iso \
  --var iso_url=./Catalina.iso \
  packer_macos.json
```

Avec une VM existante (format OVA).
```
packer build --only=virtualbox-ovf \
  --var iso_url=./macOS_Catalina.ova \
  --var cloud-token="VagrantToken" \
  packer_macos.json
```

## Post Build | Vagrant Box

__Packer__ permet de publier notre MV sur __VagrantCloud__.  Sans rentrer dans les détails, __Packer__ va créer une box (format __Vagrant__) qui n'est autre qu'un tar de notre MV avec le fichier de parametrisation.  
Par la suite, notre MV est accessible depuis __VagrantCloud__ en public et utilisable par l'outil __Vagrant__.

* Le dépôt de notre box MacOS Catalina : [GAEV/MacOS\_Catalina](https://app.vagrantup.com/GAEV/boxes/MacOS_Catalina)
* Le dépôt du projet : [https://app.vagrantup.com/GAEV](https://app.vagrantup.com/GAEV)

;-)

