#!/bin/bash

echo "export PATH=/usr/local/bin:\$PATH" >> ~/.bachrc
echo "export PATH=/usr/local/bin:\$PATH" >> ~/.zshrc
export PATH=/usr/local/bin:$PATH

echo "Download and install homebrew"
mkdir -p homebrewscript
curl -fL https://raw.githubusercontent.com/Homebrew/install/master/install.sh -o homebrewscript/install.sh
chmod u+x ./homebrewscript/install.sh
echo | ./homebrewscript/install.sh
echo | ./homebrewscript/install.sh
rm -rf homebrewscript

#echo "Install homebrew sshpass"
# otherwise install from src
#brew tap esolitos/ipa
#brew install sshpass

echo "Install python 3.x"
brew install python

