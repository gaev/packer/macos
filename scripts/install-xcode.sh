#!/bin/bash

echo "Mounting CLT dmg"
hdiutil attach Command_Line_Tools_for_Xcode.dmg
echo "Install Command Line Developer Tools"
installer -package /Volumes/Command\ Line\ Developer\ Tools/Command\ Line\ Tools.pkg -target /
echo "Unmount CLT dmg"
hdiutil detach /Volumes/Command\ Line\ Developer\ Tools
rm Command_Line_Tools_for_Xcode.dmg

echo "Install Xcode"
xip -x ~/Xcode.xip 
rm ~/Xcode.xip
sudo mv Xcode.app /Applications/Xcode.app
sudo xcode-select --switch /Applications/Xcode.app
sudo xcodebuild -license accept

echo "All done"
